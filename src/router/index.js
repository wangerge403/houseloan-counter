import Vue from 'vue'
import Router from 'vue-router'
// import home from '@/views/home/home.vue'
Vue.use(Router)

const router = new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: resolve => require(["@/views/home/home.vue"], resolve),
        },
        // {
        //     path: '/home',
        //     name: 'home',
        //     component: home
        // },
    ]
})

export default router;
export default {
  //计算公式： ［贷款本金 × 月利率 × （ 1 ＋月利率）＾还款月数］ ÷ ［（ 1 ＋月利率）＾还款月数－ 1 ］  
  //本息还款的月还款额(参数: 年利率/贷款总额/贷款总月份)
  Acpi(lilv, total, month) {
    console.log("参数", lilv, total, month)
    let rate_month = lilv / 12; //月利率
    let mounthPay = total * rate_month * Math.pow(1 + rate_month, month) / (Math.pow(1 + rate_month, month) - 1);
    return mounthPay
  },
  //本金还款的月还款额(参数: 年利率 / 贷款总额 / 贷款总月份 / 贷款当前月0～length-1)
  AverageCapital(lilv, total, month, cur_month) {
    var rate_month = lilv / 12; //月利率
    //return total * rate_month * Math.pow(1 + rate_month, month) / ( Math.pow(1 + rate_month, month) -1 );
    var benjin_money = total / month;
    return (total - benjin_money * cur_month) * rate_month + benjin_money;

  },



  //等额本息的 每月应还本金=贷款本金×月利率×(1+月利率)^(还款月序号-1)÷〔(1+月利率)^还款月数-1〕
  MonthCapital(lilv, total, month, number){
    var rate_month = lilv / 12; //月利率
    return total * rate_month * Math.pow(1+ rate_month, number - 1) / (Math.pow(1+ rate_month, month ) - 1) 

  }, 
  // 等额本金的月供本金公式  ok
  MonthCapitaldj(lilv, total, month){
    // var rate_month = lilv / 12; //月利率
    return total / month;

  }, 
  // 每月应还利息=贷款本金×月利率×〔(1+月利率)^还款月数-(1+月利率)^(还款月序号-1)〕÷〔(1+月利率)^还款月数-1〕
  MonthInerest(lilv, total, month, number){
    var rate_month = lilv / 12; //月利率
      return total * rate_month* (Math.pow(1+rate_month, month) - Math.pow(1+rate_month, number - 1)) / (Math.pow(1+rate_month, month)  - 1)
  }
}